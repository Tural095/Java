package az.blackman.studentproject.repository;

import az.blackman.studentproject.domain.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.lang.annotation.Native;
import java.util.List;

public interface StudentRepository extends JpaRepository<Student, Long>, JpaSpecificationExecutor {

    List<Student> findBySchool (String school);

    List<Student> findByNameOrHomeAddress(String name, String homeAddress);

    //JPQL

    List<Student> findByNameNamed(String name);


    @Query("SELECT s FROM Student s WHERE LOWER(s.name) = LOWER(?1) ")
    List<Student> findByCustomQuery (String name);

    @Query(nativeQuery = true, value ="SELECT s FROM students_project s WHERE LOWER(s.name) = LOWER(?1)" )
    List<Student> findByCustomNativeQuery (String name);

// native olan sql di table adi ile isleyir
    // native olmayan ise jpql de ise entity ile isleyir
}
