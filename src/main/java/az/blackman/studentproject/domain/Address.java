package az.blackman.studentproject.domain;

import lombok.*;

import javax.persistence.*;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString(exclude = "student")
@Table(name = "addresses")
public class Address {

    @Id
    @Column(name = "student_id")
    private Long id;

    private String country;

    private String city;

    private String street;

    @OneToOne
    @MapsId
    @JoinColumn(name = "student_id")
    private Student student;
}
