package az.blackman.studentproject;

import az.blackman.studentproject.domain.Student;
import az.blackman.studentproject.repository.StudentRepository;
import az.blackman.studentproject.services.ManyToManyMappingService;
import az.blackman.studentproject.services.OneToOneMappingService;
import az.blackman.studentproject.services.StudentService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@Slf4j
@RequiredArgsConstructor
@SpringBootApplication
public class StudentProjectApplication implements CommandLineRunner {

    private final StudentRepository studentRepository;
    private final StudentService studentService;
    private final OneToOneMappingService oneToOneMappingService;
    private final ManyToManyMappingService manyToManyMappingService;

    public static void main(String[] args) {
        SpringApplication.run(StudentProjectApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        log.info("Inserting data into table");
        manyToManyMappingService.createStudentsAndGroups();


        //oneToOneMappingService.createStudentAddress();


        //studentService.specExecSample();


//
//        studentRepository.findByNameNamed("Altay")
//                .stream()
//                .forEach(System.out::println);
//
//        studentRepository.findByCustomQuery("Altay")
//                .stream()
//                .forEach(System.out::println);

//		Student student = new Student();
//		student.setAge(2);
//		student.setHomeAddress("Bine");
//		student.setSchool("home");
//		student.setName("Altay");
//
//		studentRepository.save(student);
//        studentRepository.findByNameOrHomeAddress("Tural", "Baku")
//                .stream()
//                .forEach(System.out::println);
    }
}
