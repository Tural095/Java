package az.blackman.studentproject.repository;

import az.blackman.studentproject.domain.Group;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GroupRepository extends JpaRepository<Group, Long> {
}
