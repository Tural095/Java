package az.blackman.studentproject.domain;

import liquibase.pro.packaged.J;
import lombok.Data;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Data
@Entity
@Table(name = "students_project")
@NamedQuery(name = "Student.findByNameNamed", query = "SELECT s FROM Student s WHERE LOWER(s.name) = LOWER(?1) ")
public class Student {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private Integer age;

    @OneToOne(mappedBy = "student", cascade = CascadeType.ALL)
    @JoinColumn(name = "student_id")
    private Address homeAddress;

    private String school;

    private String description;

    private Long grade;

    @ManyToMany
    @JoinTable(
            name = "groups_students",
            joinColumns = {@JoinColumn(name="sid", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "gid", referencedColumnName = "id")})
    private Set<Group> groups = new HashSet<>();


}
