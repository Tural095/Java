package az.blackman.studentproject.services;

import az.blackman.studentproject.domain.Address;
import az.blackman.studentproject.domain.Student;
import az.blackman.studentproject.repository.StudentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@RequiredArgsConstructor
public class OneToOneMappingService {

    private final StudentRepository studentRepository;


    public void createStudentAddress() {
        Address address = Address.builder()
                .country("Azerbaijan")
                .city("Baku")
                .street("MKR")
                .build();


        Student student = new Student();
        student.setName("Ilkin");
        student.setAge(15);
        student.setSchool("Ingress");
        student.setHomeAddress(address);

        address.setStudent(student);
        studentRepository.save(student);

    }
}
