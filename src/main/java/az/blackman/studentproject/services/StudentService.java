package az.blackman.studentproject.services;

import az.blackman.studentproject.domain.Student;
import az.blackman.studentproject.repository.StudentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

@Service
@RequiredArgsConstructor
public class StudentService {

    private final StudentRepository studentRepository;

    private final EntityManager em;
// specification executor - ferqi coxlu metod istifade etmirsen ve dinamikdir
    public void specExecSample(){
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Student> cq = cb.createQuery(Student.class);

        Root<Student> studentRoot = cq.from(Student.class);
        Predicate predicate = cb.equal(studentRoot.get("name"), "Altay");

        Predicate predicate1 = cb.greaterThan(studentRoot.get("age"), 1);
        cq.where(predicate, predicate1);

        TypedQuery<Student> query = em.createQuery(cq);
        query.getResultList()
                .stream()
                .forEach(System.out::println);

    }
 //yuxaridaki metodun daha sade formasi
//    public void specificationExecInAction(String name){
//        studentRepository.findAll(
//                ((root, query, criteriaBuilder) ->
//                        criteriaBuilder.like(root.get("name"), "%" +name))
//        ).stream()
//                .forEach(System.out::println);
//    }
}
