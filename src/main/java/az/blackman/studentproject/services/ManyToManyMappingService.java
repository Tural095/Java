package az.blackman.studentproject.services;

import az.blackman.studentproject.domain.Group;
import az.blackman.studentproject.domain.Student;
import az.blackman.studentproject.repository.GroupRepository;
import az.blackman.studentproject.repository.StudentRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
@RequiredArgsConstructor
@Slf4j
public class ManyToManyMappingService {
    private final StudentRepository studentRepository;
    private final GroupRepository groupRepository;

    public void createStudentsAndGroups(){
        Student student = new Student();
        student.setName("Trussardi");
        student.setSchool("HP");
        student.setAge(1);

        Student student1= new Student();
        student1.setName("Armour");
        student1.setSchool("Dell");
        student1.setAge(11);

        Group MS = new Group();
        MS.setName("ms6");
        MS.setDescription("test");

        Group LMS = new Group();
        LMS.setName("ms7");
        LMS.setDescription("example");

        groupRepository.save(MS);
        groupRepository.save(LMS);

        student.setGroups(Set.of(MS, LMS));
        student1.setGroups(Set.of(MS,LMS));


        studentRepository.save(student);
        studentRepository.save(student1);
    }
}
