package az.blackman.studentproject.domain;

import liquibase.pro.packaged.E;
import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "i_groups")
public class Group {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private String description;


}
